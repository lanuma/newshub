# KLY Newshub Libary (Under Development)

![ALT](https://www.newshub.id/beta/assets/img/logo-black.png "Newshub Logo")

[![pipeline status](https://gitlab.com/lanuma/newshub/badges/v2/pipeline.svg)](https://gitlab.com/lanuma/newshub/commits/v2)
[![coverage report](https://gitlab.com/lanuma/newshub/badges/v2/coverage.svg)](https://gitlab.com/lanuma/newshub/commits/v2)
---

## How to install
Because this library is used privately by KLY programmer, we will tell composer to retrieve library from this gitlab repository.

1. Assume you have install Laravel project with composer.
2. Edit composer.json and add this part under `"license": "MIT"`:

```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/lanuma/newshub.git"
    }
],
```
Save and exit.

3. run `$ composer require lanuma/newshub:v2.x-dev`
4. After running composer command, Laravel will automatically register its service providers and facades when it is installed

## Usage

```php
<?php

namespace Project

use Lanuma\Kly\Newshub;

class NewshubTag
{
	protected $newshub;

	public function __construct(Newshub $newshub)
	{
		$this->newshub = $newshub;
	}

	public function tag()
	{
		return $this->newshub->tag()
                             ->page(1)
                             ->limit(20)
                             ->get();
	}
}
```

## Usefull Artisan command

### Migration
`$ php artisan migrate`

### Publish config
`$ php artisan vendor:publish --tag="newshub-config"`

### Publish Models
`$ php artisan vendor:publish --tag="newshub-models"`

### Publish All with single command
`$ php artisan vendor:publish --provider="Lanuma\kly\NewshubServiceProvider"`

## TODO
- [ ] Add Newshub artisan command
- [ ] [Newshub artisan command](https://gitlab.com/lanuma/newshub/issues/1)
- [x] Publish config
- [x] Publish model
- [x] Publish with ServiceProvider

## Copyright
Copyright (C) Kapanlagi Youniverse - All Rights Reserved
