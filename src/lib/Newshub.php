<?php

namespace App\Lib;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\File;

class Newshub
{
  /**
    * list api status log 
    */
  const LOG_FAIL = -1;
  const LOG_SUCCESS = 0;
  const LOG_RUNNING = 1;

  /**
    * list api section newshub
    */
  const SECTION_NEWS = 1;
  const SECTION_PHOTONEWS = 2;
  const SECTION_VIDEO = 3;
  const SECTION_CATEGORY = 4;
  const SECTION_TAG = 5;
  const SECTION_TODAY_TAG = 6;
  const SECTION_WHATSHOT = 7;
  const SECTION_BANNER = 8;
  const SECTION_FBINFO = 9;
  const SECTION_PAGEVIEW = 10;
  const SECTION_HASHTAG = 11;
  const SECTION_POPULAR = 12;
  const SECTION_QUOTE = 13;
  const SECTION_EVENT = 14;
  const SECTION_FEATURE = 15;
  const SECTION_PROFILE = 16;
  const SECTION_CREATOR = 17;
  const SECTION_CUSTOMER_SERVICE = 18;
  const SECTION_INTERACTIVE_CONTENT = 19;

  /**
   * Instance of http api di ambil dari config
   */
  protected $httpRequest;

  /**
   * Newshub Token diambil dari config
   */
  protected $newshub_token;

  /**
    * di buat untuk ambil paramater id
    */
  protected $newshubId;

  /**
    * parameter format request parameter
    */
  protected $formated_uri;

  /**
    * di buat untuk mengambil data parameter global
    * ['page', 'limit', 'last_update']
    */
  protected $httpParameter = array();

  /**
    * di buat untuk parameter grab by kategori
    */
  protected $httpChannel = '';

  public function __construct(Client $client)
  {
    $this->newshub_token = config('newshub.api_token');
    $this->httpRequest = array(
      'base_uri' => config('newshub.base_uri'),
      'timeout' => 20
    );

    $this->client = new Client($this->httpRequest);
  }

  /**
    * Channel tag
    *
    * @param array $param['page', 'limit', 'last_update']
    *
    * @return mix $this->httpChannel and $this->httpParameter
    */
  public function tag($param = null)
  {
    $this->httpChannel = 'tag';

    if (is_array($param))
    {
      // menambahkan array page
      array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
      // menambahkan array limit
      array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
      // menambahkan array last update
      array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
    }

    return $this;
  }

  /**
    * Channel event
    *
    * @param array $param['page', 'limit', 'last_update']
    *
    * @return mix $this->httpChannel and $this->httpParameter
    */
  public function event($param = null)
  {
    $this->httpChannel = 'event';

    if (is_array($param))
    {
      // menambahkan array page
      array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
      // menambahkan array limit
      array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
      // menambahkan array last update
      array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
    }

    return $this;
  }

  /**
    * Channel featured
    *
    * @param array $param['page', 'limit', 'last_update']
    *
    * @return mix $this->httpChannel and $this->httpParameter
    */
  public function featured($param = null)
  {
    $this->httpChannel = 'featured';

    if (is_array($param))
    {
      // menambahkan array page
      array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
      // menambahkan array limit
      array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
      // menambahkan array last update
      array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
    }

    return $this;
  }

  /**
   * Channel Videos
   */
  public function videos($param = null)
  {
      $this->httpChannel = 'video';

      if(is_array($param)) {
      // menambahkan array page
      array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
      // menambahkan array limit
      array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
      // menambahkan array last update
      array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
      }

      return $this;
  }

  /**
   * Channel Whatshot
   */
  public function whatshot($param = null)
  {
    $this->httpChannel = 'whatshot';

    if(is_array($param)) {
    // menambahkan array page
    array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
    // menambahkan array limit
    array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
    // menambahkan array last update
    array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
    }

    return $this;
  }

  /**
   * Channel Creator
   */
  public function creators($param = null)
  {
    $this->httpChannel = 'creator';

    if(is_array($param)) {
    // menambahkan array page
    array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
    // menambahkan array limit
    array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
    // menambahkan array last update
    array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
    }

    return $this;
  }

  /**
   * Channel banner
   */
  public function banner($param=null)
  {
    $this->httpChannel = 'banner';

    if(is_array($param)) {
    // menambahkan array page
    array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
    // menambahkan array limit
    array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
    // menambahkan array last update
    array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
    }

    return $this;
  }

  /**
   * Channel Profile
   */
  public function profile($param=null)
  {
    $this->httpChannel = 'creator';

    if(is_array($param)) {
    // menambahkan array page
    array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
    // menambahkan array limit
    array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
    // menambahkan array last update
    array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
    }

    return $this;
  }

  /**
   * Channel Profile
   */
  public function interactiveContent($param=null)
  {
    $this->httpChannel = 'interactive-content';

    if(is_array($param)) {
    // menambahkan array page
    array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
    // menambahkan array limit
    array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
    // menambahkan array last update
    array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
    }

    return $this;
  }

  /**
   * Channel CS
   */
  public function costumerService($param=null)
  {
    $this->httpChannel = 'customer-service';

    if(is_array($param)) {
    // menambahkan array page
    array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
    // menambahkan array limit
    array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
    // menambahkan array last update
    array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
    }

    return $this;
  }

  /**
   * interactive content
   */
  public function interactive_content($param=null)
  {
        $this->httpChannel = 'interactive-content';
        if(is_array($param)) {
            // menambahkan array page
            array_key_exists('page', $param) ?  $this->httpParameter = array_merge($this->httpParameter, ['page' => $param['page']]) : '';
            // menambahkan array limit
            array_key_exists('limit', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param['limit']]) : '';
            // menambahkan array last update
            array_key_exists('last_update', $param) ? $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param['last_update'])]) : '';
        }

        return $this;
  }

  /**
    * set limit grap data
    * @param string $param
    *
    * @return array $this->httpParameter
    */
  public function limit($param = null)
  {
    $this->httpParameter = array_merge($this->httpParameter, ['limit' => $param]);
    return $this;
  }

  /**
    * set page grap data
    * @param string $param
    *
    * @return array $this->httpParameter
    */
  public function page($param = null)
  {
    $this->httpParameter = array_merge($this->httpParameter, ['page' => $param]);
    return $this;
  }

  /**
    * set last update data
    * @param string $param
    *
    * @return array $this->httpParameter
    */
  public function lastUpdate($param = null)
  {
    $this->httpParameter = array_merge($this->httpParameter, ['last_update' => strtotime($param)]);
    return $this;
  }

  /**
   * Save image from newshub to cdn
   * 
   * @return string
   */
  public function save_image($image)
  {
    // $image = 'https://cdns.klimg.com/newshub.id/tag/2018/12/18/27977/infotainment-rev1.jpg';
    if(empty($image)) {
      return;
    }

    $file_name = basename($image); 
    $path = collect(explode('/', $image));
    // dump($path);
    $path->pop(); // menghapus array teakhir
    $path->forget([0,1,2,3]); // menghapus array dengan key
    $path = $path->implode('/') . '/'; // convert to string
    
    $directories = Storage::disk('_cdn_newshub');
    if (!$directories->exists($path))
    {
        $directories->makeDirectory($path, 0777, true); //creates directory
    }
    $testImage = Image::make($image)->save(config('newshub.cdn_newshub') . $path . $file_name);
    // $directories->put(config('newshub.cdn_newshub') . $path . $file_name, file_get_contents($image));
    
    $image = $path . $file_name;
    
    if($directories->exists($path . $file_name))
    {
        // dump($image);
        return $image;
    }else{
        return null;
    }
  }

  /**
    * mengambil data secara spesifik / sesuai id
    * @param string $param
    *
    * @return string $this->newshubIdr
    */
  public function newshubId($param = null)
  {
    if($param)
    {
      $this->newshubId = $param;
    }
    return $this;
  }

  /**
    * history grap parameter
    */
  public function formated_uri()
  {
    return $this->formated_uri;
  }

  public function post($data=array())
  {
      $post_uri = $this->httpChannel . '/';
      $post_uri .= '&token=' . $this->newshub_token;
      try {
        $post = $this->client->request('POST', $post_uri, ['form_params' => $data]);
      } catch (RequestException $ex) {
          if($ex->hasResponse()) {
              echo Psr7\str($e->getResponse());
          }
      }
      return $post;
  }

  /**
    * request data api
    *
    * @return JSON
    */
  public function request()
  {
    $this->formated_uri = $this->httpChannel . '/'; // build from slug
    $this->formated_uri .= (null !== $this->newshubId) ? $this->newshubId . '/' : ''; // build from id
    $this->formated_uri .= '&token=' . $this->newshub_token; // build from token

    if ($this->newshubId === null) {
      $param = http_build_query($this->httpParameter);
      $this->formated_uri .= $param ? '&' . $param : ''; // build page, last update and limit if id null
    }

    $this->formated_uri .= '?resetcache=' . microtime(true); // replace cache di newshub
    // dump($this->formated_uri);
    // https://www.newshub.id/api/tag/&token=3d7e18bf5d60838314682e45af6dde8e&page=1&limit=10
    return json_decode($this->client->request('GET', $this->formated_uri)->getBody());
  }

}
