<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Lanuma\Kly\Newshub;
use Lanuma\Kly\Newshub\Log;
use App\Whats_hot;

class NewshubWhatshot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newshub:whatshot
                            {--i|id= : whatshot id}
                            {--p|page= : Number of page that will be shown. This won\'t work if max_id parameter is exist.}
                            {--l|limit= : Total rows per page}
                            {--t|last_update= : String datetime or integer timestamp.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve whatshot from newshub';

    /**
     * Instance Newshub
     */
    protected $newshub;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Newshub $newshub)
    {
        parent::__construct();
        $this->newshub = $newshub;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Log $log)
    {
        $id         = $this->option('id');
        $page       = $this->option('page');
        $limit      = $this->option('limit');
        $lastUpdate = $this->option('last_update');

        $checkRunning = $log->check_running_status(Log::SECTION_WHATSHOT);

        if ($checkRunning !== null) {
            return $this->error('Sorry the system is still working, wait another minute... !');
        }

        $lastSuccess = $log->check_last_success(Log::SECTION_WHATSHOT);
        $lastSuccess = ($lastSuccess !== null) ? (string) strtotime($lastSuccess->created_at) : '';
        $time        = (!empty($lastUpdate)) ? $lastUpdate : $lastSuccess;

        if(!empty($id)) {
            $this->newshub->id($id);
        }else{
            $this->newshub->whatshot()
                          ->page($page)
                          ->limit($limit)
                          ->last_update($time);
        }

        // create new log
        $logId = $log->create_running_log(Log::SECTION_WHATSHOT);

        $success = 0;
        $fail = array();
        $total = 0;
        $is_next_page = true;
        while($is_next_page) {
            
            $getWhatshot = $this->newshub->get();

            if($getWhatshot === null) {
                $is_next_page = false;
                $fail['message'] = 'Error while get whatshot';
                $fail['error'] = $getWhatshot;
                $status = Log::FAIL;
            } else {
                $status = Log::SUCCESS;
                $data = array_key_exists('data', $getWhatshot) ? $getWhatshot->data : array($getWhatshot);
                $attribut = array_key_exists('attributes', $getWhatshot) ? $getWhatshot->attributes : array();

                // belum diganti
                foreach($data as $whatshot) {
                    $whatshotModel = Whats_hot::updateOrCreate([
                        'id' => $whatshot->id
                    ],[
                        'title' => $whatshot->title,
                        'order' => $whatshot->order,
                        'schedule' => $whatshot->schedule,
                        'headtorial' => $whatshot->headtorial,
                        'headtorial_schedule_end' => $whatshot->headtorial_schedule_end,
                        'image_real' => $whatshot->image->real,
                        'url' => $whatshot->url,
                        'flag' => $whatshot->flag,
                        'status' => $whatshot->status,
                    ]);

                    if($whatshotModel) {
                        $success++;
                    }else{
                        $fail['message'] = 'Error while save whatshot';
                        $fail['error'] = $whatshotModel;
                    }

                    $total++;
                }

                if(isset($attribut->next_page)){
                    $is_next_page = true;
                    $page = $attribut->next_page;
                    $this->newshub->page($page);
                }else{
                    $is_next_page = false;
                }
            }
        }

        // report
        $log->update_log($logId,[
            'total' => $total,
            'total_success' => $success,
            // 'total_fail' => ,
            'log_fail' => json_encode($fail),
            'ref_url' => $this->newshub->get_uri(),
            'status' => Log::SUCCESS,
        ]);

        $this->info("Get whatshot Success !");
        $headers = ['total data', 'success', 'fails', 'ref url'];
        $tables = [$total, $success, json_encode($fail), $this->newshub->get_uri()];
        $this->table($headers, [$tables]);
    }
}