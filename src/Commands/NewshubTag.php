<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Lanuma\Kly\Newshub;
use Lanuma\Kly\Newshub\Log;
use App\Tag;

class NewshubTag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newshub:tag
                            {--i|id= : Tag id}
                            {--p|page= : Number of page that will be shown. This won\'t work if max_id parameter is exist.}
                            {--l|limit= : Total rows per page}
                            {--t|last_update= : String datetime or integer timestamp.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve tag from newshub';

    /**
     * Instance Newshub
     */
    protected $newshub;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Newshub $newshub)
    {
        parent::__construct();
        $this->newshub = $newshub;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Log $log)
    {
        $id         = $this->option('id');
        $page       = $this->option('page');
        $limit      = $this->option('limit');
        $lastUpdate = $this->option('last_update');

        $checkRunning = $log->check_running_status(Log::SECTION_TAG);

        if ($checkRunning != null) {
            return $this->error('Sorry the system is still working, wait another minute... !');
        }

        $lastSuccess = $log->check_last_success(Log::SECTION_TAG);
        $lastSuccess = ($lastSuccess !== null) ? (string) strtotime($lastSuccess->created_at) : '';
        $time        = (!empty($lastUpdate)) ? $lastUpdate : $lastSuccess;

        if(!empty($id)) {
            $this->newshub->id($id);
        }else{
            $this->newshub->tag()
                          ->page($page)
                          ->limit($limit)
                          ->last_update($time);
        }

        // create new log
        $logId = $log->create_running_log(Log::SECTION_TAG);

        $success = 0;
        $fail = array();
        $total = 0;
        $is_next_page = true;
        while($is_next_page) {
            // get tag
            $getTag = $this->newshub->get();

            if($getTag === null) {
                $is_next_page = false;
                $fail['message'] = 'Error while get tag';
                $fail['error'] = $getTag;
                $status = Log::FAIL;
            } else {
                $status = Log::SUCCESS;
                $data = array_key_exists('data', $getTag) ? $getTag->data : array($getTag);
                $attribut = array_key_exists('attributes', $getTag) ? $getTag->attributes : array();

                foreach($data as $tag) {
                    $tag = Tag::updateOrCreate([
                        'id' => $tag->id
                    ],[
                        'name' => $tag->name,
                        'description' => $tag->description,
                        'meta_title' => $tag->meta_title,
                        'meta_description' => $tag->meta_description,
                        'meta_keyword' => $tag->meta_keyword,
                        'is_headline' => $tag->is_headline,
                        'is_smart_tag' => $tag->is_smart_tag,
                        'display_tag' => $tag->display_tag,
                        'smart_tag_type' => $tag->smart_tag_type,
                        'smart_tag_url' => $tag->smart_tag_url,
                        'related_creator' => json_encode($tag->related_creator),
                        'related_tag' => json_encode($tag->related_tag),
                        // 'image' => $this->newshub->save_image($program->image->real),
                          'image' => $tag->image->real,
                        'date_entry' => $tag->date_entry,
                        'last_update' => $tag->last_update,
                    ]);

                    if($tag) {
                        $success++;
                    }else{
                        $fail['message'] = 'Error while save tag';
                        $fail['error'] = $tag;
                    }

                    $total++;
                }

                if(isset($attribut->next_page)){
                    $is_next_page = true;
                    $page = $attribut->next_page;
                    $this->newshub->page($page);
                }else{
                    $is_next_page = false;
                }
            }
        }

        // report
        $log->update_log($logId,[
            'section' => Log::SECTION_TAG,
            'total' => $total,
            'total_success' => $success,
            // 'total_fail' => ,
            'log_fail' => json_encode($fail),
            'ref_url' => $this->newshub->get_uri(),
            'status' => Log::SUCCESS,
        ]);

        $this->info("Get tag Success !");
        $headers = ['total data', 'success', 'fails', 'ref url'];
        $tables = [$total, $success, json_encode($fail), $this->newshub->get_uri()];
        $this->table($headers, [$tables]);
    }
}
