<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Lanuma\Kly\Newshub;
use Lanuma\Kly\Newshub\Log;
use App\Banner;

class NewshubBanner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newshub:banner
                            {--i|id= : Tag id}
                            {--p|page= : Number of page that will be shown. This won\'t work if max_id parameter is exist.}
                            {--l|limit= : Total rows per page}
                            {--t|last_update= : String datetime or integer timestamp.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve banner from newshub';

    /**
     * Instance Newshub
     */
    protected $newshub;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Newshub $newshub)
    {
        parent::__construct();
        $this->newshub = $newshub;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Log $log)
    {
        $id         = $this->option('id');
        $page       = $this->option('page');
        $limit      = $this->option('limit');
        $lastUpdate = $this->option('last_update');

        $checkRunning = $log->check_running_status(Log::SECTION_BANNER);

        if ($checkRunning !== null) {
            return $this->error('Sorry the system is still working, wait another minute... !');
        }

        $lastSuccess = $log->check_last_success(Log::SECTION_BANNER);
        $lastSuccess = ($lastSuccess !== null) ? (string) strtotime($lastSuccess->created_at) : '';
        $time        = (!empty($lastUpdate)) ? $lastUpdate : $lastSuccess;

        if(!empty($id)) {
            $this->newshub->id($id);
        }else{
            $this->newshub->banner()
                          ->page($page)
                          ->limit($limit)
                          ->last_update($time);
        }

        // create new log
        $logId = $log->create_running_log(Log::SECTION_BANNER);

        $success = 0;
        $fail = array();
        $total = 0;
        $is_next_page = true;
        while($is_next_page) {
            // get banner
            $getBanner = $this->newshub->get();

            if($getBanner === null) {
                $is_next_page = false;
                $fail['message'] = 'Error while get banner';
                $fail['error'] = $getBanner;
                $status = Log::FAIL;
            } else {
                $status = Log::SUCCESS;
                $data = array_key_exists('data', $getBanner) ? $getBanner->data : array($getBanner);
                $attribut = array_key_exists('attributes', $getBanner) ? $getBanner->attributes : array();

                foreach($data as $banner) {
                    $bannerModel = Banner::updateOrCreate([
                        'id' => $banner->id
                    ],[
                        'schedule_start' => $banner->schedule_start,
                        'schedule_end' => $banner->schedule_end,
                        'title' => $banner->title,
                        'wording' => $banner->wording,
                        'image' => $banner->image,
                        'url' => $banner->url,
                        'type' => $banner->type,
                        'status' => $banner->status
                    ]);

                    if($bannerModel) {
                        $success++;
                    }else{
                        $fail['message'] = 'Error while save tag';
                        $fail['error'] = $bannerModel;
                    }

                    $total++;
                }

                if(isset($attribut->next_page)){
                    $is_next_page = true;
                    $page = $attribut->next_page;
                    $this->newshub->page($page);
                }else{
                    $is_next_page = false;
                }
            }
        }

        // report
        $log->update_log($logId,[
            // 'section' => Log::SECTION_BANNER,
            'total' => $total,
            'total_success' => $success,
            // 'total_fail' => ,
            'log_fail' => json_encode($fail),
            'ref_url' => $this->newshub->get_uri(),
            'status' => Log::SUCCESS,
        ]);

        $this->info("Get tag Success !");
        $headers = ['total data', 'success', 'fails', 'ref url'];
        $tables = [$total, $success, json_encode($fail), $this->newshub->get_uri()];
        $this->table($headers, [$tables]);
    }
}
