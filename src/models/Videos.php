<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $dates = ['news_date_publish'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'news_title', 'news_subtitle', 'news_synopsis', 'news_content', 'news_image_real', 'news_image_thumbnail',
                           'news_image_potrait', 'news_image_headline', 'news_date_publish', 'news_type', 'news_editor', 'news_paging',
                           'category', 'news_url_full', 'news_url', 'news_video', 'tag_id', 'news_tag'];
}
