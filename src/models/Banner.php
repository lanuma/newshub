<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //table
    protected $table = 'banner';

    /**
     * fillable column
     */
    protected $fillable = ['id', 'schedule_start', 'schedule_end', 'wording', 'image', 'url', 'type', 'status', 'title'];
}
