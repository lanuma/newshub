<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckQuiz extends Model
{
    

    /**
     * fillable column
     */
    protected $fillable = ['id', 'title', 'type', 'level', 'quiz_created_at'];
}
