<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
     /**
       * primary key
       */
    protected $primaryKey = 'id';

    /**
      * disable autoincrement in model
      */
    public $autoincrement = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'description', 'meta_title', 'meta_keyword', 'meta_description', 'is_headline', 'is_smart_tag', 'display_tag', 'smart_tag_type', 'smart_tag_url', 'related_creator', 'related_tag', 'image', 'date_entry', 'last_update'];

    /**
     * Relationship with Profile
     */
    public function artis()
    {
        return $this->belongsToMany('App\Profile');
    }
}
