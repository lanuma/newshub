<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_quizzes', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->integer('id');
            $table->primary('id');
            $table->string('title');
            $table->string('type');
            $table->boolean('level');
            $table->datetime('quiz_created_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_quizzes');
    }
}
