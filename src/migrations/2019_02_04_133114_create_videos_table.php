<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->string('news_title');
            $table->string('news_subtitle')->nullable();
            $table->string('news_synopsis')->nullable();
            $table->text('news_content')->nullable();
            $table->string('news_image_real');
            $table->string('news_image_thumbnail')->nullable();
            $table->string('news_image_potrait')->nullable();
            $table->string('news_image_headline');
            $table->string('news_date_publish');
            $table->string('news_type');
            $table->text('news_editor');
            $table->text('news_paging')->nullable();
            $table->text('category');
            $table->string('news_url_full');
            $table->string('news_url');
            $table->text('news_video')->nullable();
            $table->integer('tag_id')->unsigned()->nullable();
            $table->string('news_tag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
