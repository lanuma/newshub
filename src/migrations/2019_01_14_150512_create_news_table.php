<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->tinyInteger('type')->nullable();
            $table->integer('category')->nullable();
            $table->dateTime('schedule')->nullable();
            $table->string('title', 255)->nullable();
            $table->string('subtitle', 48)->nullable();
            $table->mediumText('synopsis')->nullable();
            $table->text('content')->nullable();
            $table->string('image', 255)->nullable();
            $table->string('image_thumbnail', 255)->nullable();
            $table->string('image_potrait', 255)->nullable();
            $table->tinyInteger('image_headline')->nullable();
            $table->string('image_info', 255)->nullable();
            $table->string('hastag', 255)->nullable();
            $table->string('city', 64)->nullable();
            $table->string('source', 64)->nullable();
            $table->tinyInteger('top_headline')->nullable();
            $table->tinyInteger('hot')->nullable();
            $table->tinyInteger('editor_pick')->nullable();
            $table->tinyInteger('mature')->nullable();
            $table->tinyInteger('top_headtorial')->nullable();
            $table->dateTime('date_headtorial')->nullable();
            $table->tinyInteger('paging')->nullable();
            $table->string('slug', 128)->nullable();
            $table->integer('editor')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->integer('fbinfo_like')->nullable();
            $table->integer('fbinfo_comment')->nullable();
            $table->integer('fbinfo_share')->nullable();
            $table->integer('fbinfo_commentsbox')->nullable();
            $table->integer('fbinfo_total')->nullable();
            $table->dateTime('fbinfo_last_update')->nullable();
            $table->integer('pageview_www')->nullable();
            $table->integer('pageview_m')->nullable();
            $table->integer('pageview_total')->nullable();
            $table->dateTime('pageview_last_update')->nullable();
            $table->dateTime('last_fetch_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
