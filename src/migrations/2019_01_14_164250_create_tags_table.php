<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->string('name')->nullable();
            $table->mediumText('description')->nullable();
            $table->string('meta_title', 128)->nullable();
            $table->string('meta_keyword', 128)->nullable();
            $table->string('meta_description', 128)->nullable();
            $table->boolean('is_headline');
            $table->string('is_smart_tag')->nullable();
            $table->string('display_tag')->nullable();
            $table->string('smart_tag_type')->nullable();
            $table->string('smart_tag_url')->nullable();
            $table->text('related_creator')->nullable();
            $table->text('related_tag')->nullable();
            $table->string('image', 255)->nullable();
            $table->dateTime('date_entry')->nullable();
            $table->dateTime('last_update')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
