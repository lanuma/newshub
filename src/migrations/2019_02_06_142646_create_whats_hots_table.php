<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhatsHotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whats_hots', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            
            $table->integer('id');
            $table->primary('id');
            $table->string('title');
            $table->integer('order');
            $table->datetime('schedule');
            $table->integer('headtorial');
            $table->datetime('headtorial_schedule_end');
            $table->string('image_real');
            $table->string('url');
            $table->integer('flag');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whats_hots');
    }
}
