<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagrams', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            
            $table->bigInteger('id');
            $table->primary('id')->comment('POST ID');
            $table->integer('user_id')->commant('OWNER USER ID');
            $table->mediumText('image_thumbnail')->commant('IMAGE THUMBNAIL');
            $table->mediumText('image_low')->commant('IMAGE LOW RESOLUTION');
            $table->mediumText('image_standard')->commant('IMAGE STANDART RESOLUTION');
            $table->text('caption')->commant('POST CAPTION');
            $table->mediumText('link')->commant('LINK POST');
            $table->tinyInteger('has_carousel')->commant('HAVE CAROUSEL OR NOT ?')->nullable();
            $table->datetime('created_time')->commant('DATE CREATED');
            $table->tinyInteger('status')->commant('STATUS')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagrams');
    }
}
