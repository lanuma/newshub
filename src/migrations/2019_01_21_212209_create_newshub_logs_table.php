<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewshubLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newshub_logs', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            
            $table->increments('id')->unsigned();
            $table->string('section');
            $table->integer('total')->nullable();
            $table->integer('total_success')->nullable();
            $table->integer('total_fail')->nullable();
            $table->text('log_fail')->nullable();
            $table->string('ref_url', 255)->nullable();
            $table->tinyInteger('status')->comment('1=running;0=done;-1=kill');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newshub_logs');
    }
}
