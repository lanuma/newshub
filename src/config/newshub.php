<?php

return [
	'api_url' => env('NEWSHUB_API_URL', 'https://www.newshub.id/api/'),
	'token' => env('NEWSHUB_TOKEN', ''),
];